import os.path
from django.db import models
from django.core.files.storage import FileSystemStorage
from broker import settings


fs = FileSystemStorage(location=os.path.join(settings.base.BASE_DIR,"broker_opens","scripts","csv_importer"))

class CsvUpload(models.Model):
    file = models.FileField(storage=fs, upload_to="uploaded")
    details = models.CharField(max_length=255, blank=True)
    dump = models.TextField(blank=True)

    class Meta:
        verbose_name_plural = 'csvuploads'
        app_label = 'broker_opens'
        db_table = 'CsvUpload'

