from django.db import models


class BrokerOpen(models.Model):
    date = models.DateField(db_index=True)
    location = models.CharField(db_index=True, max_length=50)
    active = models.BooleanField(db_index=True, default=False)
    deleted = models.BooleanField(db_index=True, default=False)

    @property
    def url_friendly_location(self):
        return self.location.replace(' ','_')

    @property
    def url_to_this(self):
        return '/%s/%s' % (self.date.strftime('%m/%d/%y'), self.url_friendly_location)

    @classmethod
    def get_next_sort_value(cls, id):
        from property import Property
        if Property.objects.filter(broker_open_id=id).count() > 0:
            max = Property.objects.filter(broker_open_id=id).aggregate(models.Max('sort_order'))['sort_order__max']
            return max+1
        else:
            return 1

    class Meta:
        verbose_name_plural = 'brokeropens'
        app_label = 'broker_opens'
        db_table = 'BrokerOpen'

    def __unicode__(self):
        return '%s - %s' % (self.date.strftime('%m/%d/%y'), self.location)


