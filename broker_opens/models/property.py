import random
import string

from django.conf import settings
from django.db import models
from broker_open import BrokerOpen

class Property(models.Model):

    mls_id = models.CharField(primary_key=True, max_length=10)
    start_time = models.TimeField(auto_now=False, auto_now_add=False)
    end_time = models.TimeField(auto_now=False, auto_now_add=False)
    address = models.CharField(max_length=255)
    city = models.CharField(max_length=75)
    zip_code = models.IntegerField(blank=True, max_length=5)
    latitude = models.DecimalField(blank=True, null=True, max_digits=9, decimal_places=6)
    longitude = models.DecimalField(blank=True, null=True, max_digits=9, decimal_places=6)
    beds = models.IntegerField(blank=True, max_length=2)
    baths = models.IntegerField(blank=True, max_length=2)
    price = models.IntegerField(blank=True, max_length=8)
    listing_agent = models.ForeignKey(settings.AUTH_USER_MODEL)
    property_type = models.CharField(blank=True, max_length=150)
    days_on_market = models.IntegerField(blank=True, null=True, max_length=5)
    edited_by_user = models.BooleanField(default=False)
    broker_open = models.ForeignKey(BrokerOpen)
    sort_order = models.IntegerField(max_length=3)

    @classmethod
    def create_property_with_random_id(cls):
        """ Generates a random property id in the form of CUST_XXXXX
        """
        string_valid = False
        while string_valid == False:
            random_chars = ''.join(random.choice(string.ascii_uppercase+string.digits) for x in range(5))
            rand_string = "CUST_" + random_chars
            try:
                cls.objects.get(mls_id=rand_string)
            except:
                string_valid = True
        return cls(mls_id=rand_string)

    def created_by_user(self):
        if "CUST_" in self.mls_id:
            return True
        else:
            return False


    class Meta:
        verbose_name_plural = 'properties'
        app_label = 'broker_opens'
        db_table = 'Property'
