from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils import timezone
from django.core.mail import send_mail


class AgentManager(BaseUserManager):
    def create_user(self, email, password=None, **extra_fields):
        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')
        user = Agent(email=email, is_staff=False, is_active=True, is_superuser=False, last_login=now, date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user


    def create_superuser(self, email, password, **extra_fields):
        u = self.create_user(email, password, **extra_fields)
        u.is_staff = True
        u.is_active = True
        u.is_superuser = True
        u.save(using=self._db)
        return u


class Agent(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True, max_length=254)
    mls_id = models.CharField(unique=True, max_length=15)
    first_name = models.CharField(db_index=True, blank=True, max_length=50)
    last_name = models.CharField(db_index=True, blank=True, max_length=50)
    office_id = models.CharField(max_length=15)
    office_name = models.CharField(blank=True, max_length=50)
    phone_number = models.CharField(max_length=12)
    name = models.CharField(db_index=True, blank=True, max_length=50)
    is_staff = models.BooleanField(default=False)
    is_moderator = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)

    objects = AgentManager()

    USERNAME_FIELD = 'email'

    class Meta:
        verbose_name_plural = 'agents'
        app_label = 'broker_opens'
        db_table = 'Agent'

    def get_full_name(self):
        return '%s %s' % (self.first_name, self.last_name)

    def get_short_name(self):
        return self.first_name

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    def email_user(self, subject, message, from_email=None, **kwargs):
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def __unicode__(self):
        return self.last_name_first()

    @property
    def username(self):
        return getattr(self, self.USERNAME_FIELD)

    @property
    def name(self):
        return '%s %s' % (self.first_name, self.last_name)

    def last_name_first(self):
        return '%s, %s' % (self.last_name, self.first_name)

    @property
    def user_personal_info_valid(self):
        # Just check that the information exists for now
        if self.first_name and self.last_name and self.office_name and self.phone_number:
            return True
        else:
            return False

