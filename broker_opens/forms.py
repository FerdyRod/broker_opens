from __future__ import unicode_literals
import datetime
from django import forms
from django.forms import ModelForm
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from models import Agent, Property, BrokerOpen


class AgentCreationForm(forms.Form):
    """
    A form that creates a user, with no privileges, from the given email and
    password.
    """
    error_messages = {
        'duplicate_email': _("A user with that  email already exists."),
        'password_mismatch': _("The two password fields didn't match."),
    }
    email = forms.EmailField(label=_("Email"),
                             widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Email'}))

    mls_id = forms.CharField(label=_("MLS_ID"),
                             widget=forms.TextInput(attrs={'class':'form-control','placeholder':'MLS ID'}))

    password1 = forms.CharField(label=_("Password"),
                                widget=forms.PasswordInput(attrs={'class':'form-control','placeholder':'Password'}),)

    password2 = forms.CharField(label=_("Password confirmation"),
                                widget=forms.PasswordInput(attrs={'class':'form-control','placeholder':'Confirm Password'}),
                                help_text=_("Enter the same password as above, for verification."),
                                )


    #class Meta:
    #    model = Agent
    #    fields = ("email",)

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'])
        return password2

    def save(self, commit=True):
        user = super(AgentCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class AgentChangeForm(forms.ModelForm):
    email = forms.EmailField(label=_("Email"))

    password = ReadOnlyPasswordHashField(label=_("Password"),
                                         help_text=_("Raw passwords are not stored, so there is no way to see "
                                                     "this user's password, but you can change the password "
                                                     "using <a href=\"password/\">this form</a>."))

    class Meta:
        model = Agent

    def __init__(self, *args, **kwargs):
        super(AgentChangeForm, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

class PasswordResetRequestForm(forms.Form):
    email = forms.CharField(label=_("Email"))

    def clean_email(self):
        email = self.cleaned_data.get("email")
        if not Agent.objects.filter(email=email).exists():
            raise forms.ValidationError(_("No user with that email exists"))
        return email

class PasswordResetForm(forms.Form):

    password1 = forms.CharField(label=_("Password"),
                                widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password confirmation"),
                                widget=forms.PasswordInput)

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(_("The two passwords did not match"))
        return password2


class EditAgentProfileForm(forms.Form):
    first_name = forms.CharField(max_length=50)
    last_name = forms.CharField(max_length=50)
    phone_number = forms.CharField(max_length=12)
    office_name = forms.CharField(max_length=50)


class CreateAgentProfileForm(forms.Form):
    mls_id = forms.CharField(max_length=15)
    first_name = forms.CharField(required=False, max_length=50)
    last_name = forms.CharField(required=False, max_length=50)
    phone_number = forms.CharField(required=False, max_length=12)
    office_name = forms.CharField(required=False, max_length=50)
    is_active = forms.BooleanField(required=False, widget=forms.CheckboxInput(attrs={'class':'form-control'}))
    email = forms.EmailField()

    def clean_mls_id(self):
        cleaned_data = super(CreateAgentProfileForm,self).clean()
        try:
            Agent.objects.get(mls_id=cleaned_data['mls_id'])
            self._errors['mls_id'] = self.error_class(['This MLS id is already in the system.'])
        except Agent.DoesNotExist:
            pass
        return cleaned_data['mls_id']

    def clean_email(self):
        cleaned_data = super(CreateAgentProfileForm,self).clean()
        try:
            Agent.objects.get(email=cleaned_data['email'])
            self._errors['email'] = self.error_class(['This email id is already in the system.'])
        except Agent.DoesNotExist:
            pass
        return cleaned_data['email']



class ModeratorEditAgentProfileForm(forms.Form):
    first_name = forms.CharField(required=False, max_length=50)
    last_name = forms.CharField(required=False, max_length=50)
    phone_number = forms.CharField(required=False, max_length=12)
    office_name = forms.CharField(required=False, max_length=50)
    is_active = forms.BooleanField(required=False, widget=forms.CheckboxInput(attrs={'class':'form-control'}))
    email = forms.EmailField()

    def set_user_id(self, user_id):
        """
        Here we an pass in user_id data so that we can use it later to validate the user's email address.
        """
        self.user_id = user_id

    def clean_email(self):
        cleaned_data = super(ModeratorEditAgentProfileForm,self).clean()
        try:
            if hasattr(self, 'user_id'):
                Agent.objects.exclude(mls_id=self.user_id).get(email=cleaned_data['email'])
            else:
                Agent.objects.get(email=cleaned_data['email'])
            self._errors['email'] = self.error_class(['This email already exists in the system.'])
        except Agent.DoesNotExist:
            pass
        return cleaned_data['email']


times = [
    ("09:00:00","9:00AM"),
    ("09:15:00","9:15AM"),
    ("09:30:00","9:30AM"),
    ("09:45:00","9:45AM"),
    ("10:00:00","10:00AM"),
    ("10:15:00","10:15AM"),
    ("10:30:00","10:30AM"),
    ("10:45:00","10:45AM"),
    ("11:00:00","11:00AM"),
    ("11:15:00","11:15AM"),
    ("11:30:00","11:30AM"),
    ("11:45:00","11:45AM"),
    ("12:00:00","12:00AM"),
    ("12:15:00","12:15AM"),
    ("12:30:00","12:30AM"),
    ("12:45:00","12:45AM"),
    ("13:00:00","1:00PM"),
    ("13:15:00","1:15PM"),
    ("13:30:00","1:30PM"),
    ("13:45:00","1:45PM"),
    ("14:00:00","2:00PM"),
    ("14:15:00","2:15PM"),
    ("14:30:00","2:30PM"),
    ("14:45:00","2:45PM"),
    ("15:00:00","3:00PM"),
    ("15:15:00","3:15PM"),
    ("15:30:00","3:30PM"),
    ("15:45:00","3:45PM"),


]

property_types = [
    ("SFR","Single Family Residence"),
    ("CND","Condo"),
    ("TWN", "Townhome"),
]


class BrokerEditListingForm(forms.Form):

    address = forms.CharField(max_length=255)
    city = forms.CharField(max_length=75)
    zip_error = 'Please enter a valid CA zip code between 90001 and 96162.'
    zip_code = forms.IntegerField(min_value=90001, max_value=96162, widget=forms.TextInput,\
                                  error_messages={'min_value':zip_error, 'max_value':zip_error})
    beds = forms.IntegerField(max_value=99, min_value=1)
    baths = forms.IntegerField(max_value=99, min_value=1)
    price = forms.IntegerField(max_value=99999999, min_value=1, widget=forms.TextInput)
    property_type = forms.ChoiceField(required=True, choices=property_types, widget=forms.Select(attrs={'class':'form-control'}) )
    broker_open = forms.ModelChoiceField(queryset= BrokerOpen.objects.filter(deleted=False, active=True), empty_label=None, widget=forms.Select(attrs={'class':'form-control'}))
    start_time = forms.ChoiceField(required=False, choices=times, widget=forms.Select(attrs={'class':'form-control'}))
    end_time = forms.ChoiceField(required=False, choices=times, widget=forms.Select(attrs={'class':'form-control'}))

    def clean(self):
        cleaned_data = super(BrokerEditListingForm,self).clean()
        if cleaned_data['start_time'] >= cleaned_data['end_time']:
            self._errors['end_time'] = self._errors['start_time'] = self.error_class(['End time must come after start time.'])
            del cleaned_data['start_time']
        return cleaned_data


class ModeratorEditListingForm(BrokerEditListingForm):
    days_on_market = forms.IntegerField(required=False, max_value=999)
    latitude = forms.DecimalField(required=False, max_value=180, min_value=-180)
    longitude = forms.DecimalField(required=False, max_value=180, min_value=-180)
    edited_by_user = forms.BooleanField(required=False, widget=forms.CheckboxInput(attrs={'class':'form-control'}))
    sort_order = forms.IntegerField()
    listing_agent = forms.ModelChoiceField(queryset= Agent.objects.filter().order_by('last_name'), empty_label=None, widget=forms.Select(attrs={'class':'form-control'}))
    broker_open = forms.ModelChoiceField(queryset= BrokerOpen.objects.filter(deleted=False), empty_label=None, widget=forms.Select(attrs={'class':'form-control'}))



class ContactUsForm(forms.Form):
    name = forms.CharField(max_length=100, required=True)
    email = forms.EmailField(max_length=254, required=True)
    phone = forms.CharField(required=False, max_length=12)
    newsletter = forms.BooleanField(required=False, initial=False)
    body = forms.CharField(required=True)


class EditBrokerOpenForm(forms.Form):
    date = forms.DateField(required=True)
    location = forms.CharField(required=True, max_length=50)
    active = forms.BooleanField(required=False, widget=forms.CheckboxInput(attrs={'class':'form-control'}) )


class NewBrokerOpenForm(ModelForm):
    date = forms.DateField(input_formats=['%m/%d/%Y',])
    active = forms.BooleanField(required=False, widget=forms.CheckboxInput(attrs={'class':'form-control'}) )
    class Meta:
        model = BrokerOpen
        exclude = {'deleted'}
