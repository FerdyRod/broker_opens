import datetime
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth import login, logout, authenticate
from django.http import Http404, HttpResponseRedirect
from django.contrib import messages
from django.db.models import Max
from django.core.mail import send_mail
from django.conf import settings
from django.forms.models import model_to_dict
from django.utils.datastructures import MultiValueDictKeyError
from django.core.urlresolvers import reverse

from broker_opens.models import Agent, BrokerOpen, Property, CsvUpload
from broker_opens.forms import EditAgentProfileForm, BrokerEditListingForm, ModeratorEditListingForm, ContactUsForm, EditBrokerOpenForm, NewBrokerOpenForm
from broker_opens.scripts.csv_importer import file_handler

def property_list(request, day=None, month=None, year=None, location=""):
    date_string = day + '/' + month + '/' + year
    date_object = datetime.datetime.strptime(date_string, "%d/%m/%y").date()
    broker_open = BrokerOpen.objects.get(date__contains=date_object, location__iexact=location.replace('_',' '))
    property_list = broker_open.property_set.all().order_by('sort_order').select_related('listing_agent')
    context = {'broker_open': broker_open, 'property_list': property_list}
    return render(request, 'properties/property_list.html', context)


@login_required
def users_listings(request):
    agent = request.user
    properties = Property.objects.all().filter(listing_agent=agent.id)
    context = {'agent': agent, 'properties': properties}
    return render(request, 'properties/agent_listing.html', context)


def _edit_listing_broker(request, listing):
    if listing.listing_agent.mls_id != request.user.mls_id:
        raise Http404

    if request.method == 'POST':
        form = BrokerEditListingForm(request.POST)
        if form.is_valid():
            listing.__dict__.update(form.cleaned_data)
            listing.edited_by_user = True
            listing.broker_open_id = form.cleaned_data['broker_open'].id
            listing.save()

            messages.add_message(request,messages.INFO, 'Your listing has been updated.')
            return redirect('/my_listings/')
    else:
        form = BrokerEditListingForm(model_to_dict(listing))


    context = {'listing': listing, 'form': form, 'page_title':'Edit Listing - %s'%listing.mls_id}
    return render(request, 'properties/edit_listing_broker.html', context)


def _edit_listing_moderator(request, listing):

    if request.method == 'POST':
        form = ModeratorEditListingForm(request.POST)
        if form.is_valid():
            listing.__dict__.update(form.cleaned_data)
            listing.broker_open_id = form.cleaned_data['broker_open'].id
            listing.save()

            messages.add_message(request,messages.INFO, 'The listing has been updated.')
            return redirect(listing.broker_open.url_to_this)
    else:
        form = ModeratorEditListingForm(model_to_dict(listing))


    context = {'listing': listing, 'form': form, 'page_title':'Edit Listing - %s'%listing.mls_id}
    return render(request, 'properties/edit_listing_moderator.html', context)


@login_required
def edit_listing(request, mls_id):
    listing = get_object_or_404(Property, mls_id=mls_id)
    # make sure user is allowed to edit
    if not request.user.is_moderator:
        return _edit_listing_broker(request, listing)
    else:
        return _edit_listing_moderator(request,listing)



@login_required
def create_users_listing(request):
    if not request.user.user_personal_info_valid:
        messages.add_message(request,messages.INFO, 'Please enter your information before creating a listing.')
        return redirect(reverse('edit_profile'))

    if request.method == 'POST':
        form = BrokerEditListingForm(request.POST)
        if form.is_valid():

            listing = Property.create_property_with_random_id()
            listing.listing_agent_id = request.user.id
            listing.edited_by_user = True
            listing.__dict__.update(form.cleaned_data)
            listing.broker_open_id = form.cleaned_data['broker_open'].id
            listing.sort_order = BrokerOpen.get_next_sort_value(form.cleaned_data['broker_open'].id)
            listing.save()
            messages.add_message(request,messages.INFO, 'Your listing has been created.')
            return redirect('/my_listings/')
    else:
        form = BrokerEditListingForm()
    context = {'form': form, 'page_title':'Create New Listing'}
    return render(request, 'properties/edit_listing_broker.html', context)


@login_required
@user_passes_test(lambda x: x.is_moderator)
def moderator_create_listing(request, broker_open_id):
    broker_open = get_object_or_404(BrokerOpen, pk=broker_open_id)
    if request.method == 'POST':
        form = ModeratorEditListingForm(request.POST)
        if form.is_valid():

            listing = Property.create_property_with_random_id()
            listing.listing_agent_id = request.user.id
            listing.edited_by_user = True
            listing.__dict__.update(form.cleaned_data)
            listing.broker_open_id = form.cleaned_data['broker_open'].id
            listing.sort_order = BrokerOpen.get_next_sort_value(form.cleaned_data['broker_open'].id)
            listing.save()
            messages.add_message(request,messages.INFO, 'Your listing has been created.')
            return redirect('/my_listings/')
    else:
        form = ModeratorEditListingForm(initial={'broker_open':broker_open})
    context = {'form': form, 'page_title':'Create New Listing'}
    return render(request, 'properties/edit_listing_moderator.html', context)


@login_required
@user_passes_test(lambda x: x.is_moderator)
def upload_csv(request, broker_open_id):
    broker_open = get_object_or_404(BrokerOpen, pk=broker_open_id)
    if request.method == 'POST':
        message = ""
        try:
            csv = CsvUpload(file=request.FILES['csv_file'])
            csv.save()

            result = file_handler.process_file(request.FILES['csv_file'], broker_open.id)
            if result['success']:
                message += "<p>" + "Your File Was Successfully Imported." + "</p>"
            else:
                message += "<p>" + "An Error Occured While Importing." + "</p>"
            if result['details']:
                message += "<hr><p>" + result['details'] + "</p>"
            if result['dump']:
                message += "<hr><p>" + result['dump'] + "</p>"
            csv.save()
            csv.details = result['details']
            csv.dump = result['dump']
            csv.save()
        except MultiValueDictKeyError:
            message += "<p>" + "Please Upload a File" + "</p>"

        except:
            message += "<p>" + "An Unknown Error Occured" + "</p>"

        messages.add_message(request,messages.INFO, message)


    return redirect(broker_open.url_to_this)


