from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.conf import settings

from broker_opens.forms import ContactUsForm


def contact(request):
    if request.method == "POST":
        form = ContactUsForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            phone = form.cleaned_data['phone']
            newsletter = form.cleaned_data['newsletter']
            body = form.cleaned_data['body']
            subject = 'New Inquiry From Broker Opens Portal'
            message = 'name: ' + name + '\n' + 'phone: ' + phone + '\n' + 'email: ' + email + '\n' + 'subscribe to newsletter?: ' + str(newsletter)
            if not request.user.is_anonymous():
                message += '\nuser ID: %s' % request.user.id
                message += '\nuser name: %s' % request.user.name

            message += '\n\n' + body
            send_mail(subject, message, settings.EMAIL_HOST_USER, ['jasonscotthamilton@gmail.com'])
            return redirect('/thanks/')
    else:
        form = ContactUsForm()
    context = {'form': form}
    return render(request, 'contact/contact.html', context)


def thanks(request):
    return render(request, 'contact/thanks.html')