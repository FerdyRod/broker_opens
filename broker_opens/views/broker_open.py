from django.shortcuts import render, get_object_or_404, redirect
from django.template import RequestContext
from django.contrib.auth.decorators import login_required, user_passes_test

from broker_opens.models import BrokerOpen
from broker_opens.forms import EditBrokerOpenForm, NewBrokerOpenForm


def home(request):
    if request.user.is_anonymous():
        broker_opens = BrokerOpen.objects.filter(deleted=False, active=True)\
            .order_by('date','location')
    elif request.user.is_moderator:
        broker_opens = BrokerOpen.objects.filter(deleted=False)\
            .order_by('date','location')
    else:
        broker_opens = BrokerOpen.objects.filter(deleted=False, active=True)\
            .order_by('date','location')

    context = {'broker_opens': broker_opens}
    return render(request, 'broker_opens/home.html', context, context_instance=RequestContext(request))


@login_required
@user_passes_test(lambda x: x.is_moderator)
def new_broken_open(request):
    if request.method == "POST":
        form = NewBrokerOpenForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
    else:
        form = NewBrokerOpenForm()
    context = {'form': form}
    return render(request, 'broker_opens/edit_broker_open.html', context)


@login_required
@user_passes_test(lambda x: x.is_moderator)
def edit_broker_open(request, id):
    broker_open = BrokerOpen.objects.get(pk=id)
    if request.method == "POST":
        form = EditBrokerOpenForm(request.POST)
        if form.is_valid():
            print form.cleaned_data
            broker_open.__dict__.update(form.cleaned_data)
            print broker_open.__dict__
            broker_open.save()
            return redirect('/')
    else:
        form = EditBrokerOpenForm(broker_open.__dict__)
    context = {'broker_open':broker_open, 'form': form}
    return render(request, 'broker_opens/edit_broker_open.html', context)


@login_required
@user_passes_test(lambda x: x.is_moderator)
def delete_broker_open(request, id):
    broker_open = BrokerOpen.objects.get(pk=id)
    broker_open.deleted = True
    broker_open.save()
    return redirect('/')
