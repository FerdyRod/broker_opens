from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth import login, logout, authenticate
from django.contrib import messages
from django.core.urlresolvers import reverse

from broker_opens.models import Agent
from broker_opens.forms import EditAgentProfileForm, ModeratorEditAgentProfileForm, CreateAgentProfileForm


def log_in(request):
    logout(request)
    username = password = ''
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username.lower(), password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                if user.is_moderator:
                    return redirect('/')
                else:
                    return redirect('/my_listings/')

    return render(request, 'users/login.html')


@login_required
@user_passes_test(lambda x: x.is_moderator)
def list_agents(request):
    agents = Agent.objects.all().order_by('last_name')
    context = {'agents': agents}
    return render(request, 'users/agents_list.html', context)


@login_required
def edit_users_profile(request):
    agent = Agent.objects.get(mls_id=request.user.mls_id)
    if request.method == 'POST':
        form = EditAgentProfileForm(request.POST)
        if form.is_valid():
            agent.__dict__.update(form.cleaned_data)
            agent.save()
            messages.add_message(request,messages.INFO, 'Your profile has been updated.')
            return redirect('/my_listings/')
    else:
        form = EditAgentProfileForm(agent.__dict__)

    context = {'agent': agent, 'form': form}
    return render(request, 'users/edit_agent_profile.html', context)



@login_required
@user_passes_test(lambda x: x.is_moderator)
def moderator_edit_users_profile(request, user_id):
    agent = Agent.objects.get(mls_id=user_id)
    if request.method == 'POST':
        form = ModeratorEditAgentProfileForm(request.POST)
        form.set_user_id(user_id)
        if form.is_valid():
            agent.__dict__.update(form.cleaned_data)
            agent.save()
            messages.add_message(request,messages.INFO, 'The agent profile has been updated.')
            return redirect(request.GET.get('next','/'))
    else:
        form = ModeratorEditAgentProfileForm(agent.__dict__)
        form.set_user_id(user_id)

    context = {'agent': agent, 'form': form}
    return render(request, 'users/edit_agent_profile.html', context)



@login_required
@user_passes_test(lambda x: x.is_moderator)
def moderator_create_user(request):

    if request.method == 'POST':
        form = CreateAgentProfileForm(request.POST)
        if form.is_valid():
            agent = Agent()
            print form.cleaned_data['mls_id']
            agent.__dict__.update(form.cleaned_data)
            agent.save()
            messages.add_message(request,messages.INFO, 'The agent profile has been created.')
            return redirect(reverse('agent_list'))
    else:
        form = CreateAgentProfileForm()

    context = {'form': form}
    return render(request, 'users/create_agent.html', context)