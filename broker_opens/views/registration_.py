
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, logout, authenticate
from django.contrib.sites.models import RequestSite
from django.contrib.sites.models import Site
from django.contrib import messages
from django.http import Http404, HttpResponseRedirect
from django.db.models import Max, Q
from django.db import IntegrityError
from django.core.mail import send_mail
from django.conf import settings
from django.forms.models import model_to_dict
from django.utils.encoding import force_text
from django.core.urlresolvers import reverse_lazy

from registration import signals
from registration.models import RegistrationProfile
from registration.views import RegistrationView as BaseRegistrationView
from registration.views import ActivationView as BaseActivationView

from broker_opens.models import Agent, BrokerOpen, Property
from broker_opens.forms import (EditAgentProfileForm, BrokerEditListingForm, ContactUsForm,
                  EditBrokerOpenForm, NewBrokerOpenForm, AgentCreationForm)



def verification(request):
    return render(request, 'registration/verification.html')


class RegistrationView(BaseRegistrationView):
    """Overriding django-registration Class Based View to do signup logic"""
    form_class = AgentCreationForm

    def register(self, request, **cleaned_data):

        email, mls_id, password = cleaned_data['email'].lower(), cleaned_data['mls_id'].upper(), cleaned_data['password1']

        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(request)

        try:
            user = Agent.objects.get(Q(email=email) | Q(mls_id=mls_id))
            if user:
                if user.email == email and user.mls_id == mls_id and user.is_active == False:
                    # If user exists in DB and isn't active, create activation Key and send email
                    try:
                        new_user = RegistrationProfile.objects.create_profile(user)
                        new_user.send_activation_email(site)
                        signals.user_registered.send(sender=self.__class__,
                                                 user=new_user,
                                                 request=request)
                        user.set_password(password)
                        user.save()
                    except IntegrityError:
                        pass


                else:
                    mod_emails = []
                    moderators = Agent.objects.filter(is_moderator=True)
                    for moderator in moderators:
                        mod_emails.append(moderator.email)
                    subject = 'User signed up with the same email or MLS ID'
                    message = 'A user signed up with the following information: \n email: ' + email + \
                              '\n mls_id: ' + mls_id + '\n\n Existing User: \n email: ' + \
                              user.email + '\n mls_id: ' + user.mls_id + \
                              "\n\nYou will need to manually mark this user's account as active and have them follow the password reset link to activate this account"
                    send_mail(subject, message, settings.EMAIL_HOST_USER, mod_emails)
                    self.success_url = 'verifying_account'

        except Agent.DoesNotExist:
            user = Agent.objects.create_user(email=email, password=password)
            user.mls_id = mls_id
            user.is_active = False
            user.save()

            #send email to moderator so they can login and confirm user
            mod_emails = []
            moderators = Agent.objects.filter(is_moderator=True)
            for moderator in moderators:
                mod_emails.append(moderator.email)
            subject = 'A new user signed up and needs to be approved.'
            message = 'A user signed up with the following information: \n email: ' + email + '\n mls_id: ' + mls_id +\
                      '\n\nThis user is not recognized so you will need to approve this user manually.'
            send_mail(subject, message, settings.EMAIL_HOST_USER, mod_emails)

            self.success_url = 'verifying_account'

        return user


    def get_success_url(self, request, user):
        """
        Return the name of the URL to redirect to after successful
        user registration.

        """
        if self.success_url:
            #Forcing possible reverse_lazy evaluation
            return force_text(self.success_url)
        return ('registration_complete', (), {})


class ActivationView(BaseActivationView):
    """CBV for activating the user"""

    def activate(self, request, activation_key):
        activated_user = RegistrationProfile.objects.activate_user(activation_key)
        if activated_user:
            signals.user_activated.send(sender=self.__class__,
                                        user=activated_user,
                                        request=request)
        return activated_user


    def get_success_url(self, request, user):
        if user:
            context = {'account': user}
        else:
            context = {}
        success_url = reverse_lazy('registration_activation_complete')
        return (success_url, (), context)