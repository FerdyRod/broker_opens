from string import upper

from django import http
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.exceptions import ImmediateHttpResponse

from broker_opens.models import Property, Agent


class CORSResource(object):
    """
    Adds Cross-origin resource sharing headers to resources that subclass this.
    """
    def create_response(self, *args, **kwargs):
        response = super(CORSResource, self).create_response(*args, **kwargs)
        response['Access-Control-Allow-Origin'] = '*'
        response['Access-Control-Allow-Headers'] = 'Content-Type'
        return response

    def method_check(self, request, allowed=None):
        if allowed is None:
            allowed = []
        request_method = request.method.lower()
        allows = ['GET', 'POST', 'PUT', 'DELETE', 'PATCH']

        if request_method == 'options':
            response = HttpResponse(allows)
            response['Access-Control-Allow-Origin'] = '*'
            response['Access-Control-Allow-Headers'] = 'Content-Type'
            response['Allow'] = allows
            raise ImmediateHttpResponse(response=response)

        if not request_method in allowed:
            response = http.HttpMethodNotAllowed(allows)
            response['Allow'] = allows
            raise ImmediateHttpResponse(response=response)

        return request_method


class AgentResource(CORSResource, ModelResource):
    class Meta:
        queryset = Agent.objects.all()
        resource_name = 'listing_agent'
        fields = ['first_name', 'last_name', 'office_name', 'phone_number']


class PropertyResource(CORSResource, ModelResource):
    listing_agent = fields.ForeignKey(AgentResource, 'listing_agent', full=True)

    class Meta:
        queryset = Property.objects.all()
        resource_name = 'property'
        fields = ['address', 'baths', 'beds', 'city', 'days_on_market',
                  'end_time', 'mls_id', 'price', 'property_type', 'start_time']