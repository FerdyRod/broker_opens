
import csv
import string
from datetime import datetime
from django.utils import timezone

from broker_opens.models import Agent
from broker_opens.models import Property
from broker_opens.models import BrokerOpen



REQUIRED_FIELD = ["Listing Agent Public ID", "Listing Agent First Name",
                    "Listing Agent Last Name", "Listing Office Code", "Listing Office Name",
                    "Listing Agent Email Address", "Listing Agent Cell Phone", "Street Direction",
                    "Street Direction Suffix", "Street Name", "Street Number", "Street Number Modifier",
                    "Street Suffix", "Street Suffix Modifier", "Unit Number", "Multiple Listing Number",
                    "City", "Zip Code", "Latitude", "Longitude", "Bedrooms", "Baths Total",
                    "Current Price", "List Price", "Type", "Days on Market"
                   ]



#################################################################
######################  HELPER FUNCTIONS    #####################
#################################################################


def issublist(list1, list2):
    """ Returns True if all items of list1 are in list2. Else returns False
    """
    return set(list1).issubset(set(list2))

def items_missing(required, items_available):
    return [i for i in required if i not in items_available]

def normalize_number(a):
    """Convert string to int
    """
    if a.isdigit():
        return a
    else:
        return 0

def int_or_null(a):
    if a.isdigit():
        return a
    else:
        return None


def int_or_zero(a):
    if a:
        return a
    else:
        return 0


def decimal_or_null(a):
    if a:
        return a
    else:
        return None

def str_to_datetime(date):
    """Convert string to datetime
    """
    try:
        return datetime.strptime(date, "%m/%d/%Y %H:%M:%S %p")
    except:
        return None

def convert_structure_attached_field(value):
    if value.lower() == 'attached':
        return True
    if value.lower() == 'detached':
        return False
    else:
        return None
        
def gen_address(street_direction, street_direction_suffix, street_name, street_number,
                 street_number_modifier, street_suffix, street_suffix_modifier, unit_number):
    """Return an address from some parts
    """
    address = ""
    if street_number:
        address += street_number
    if street_number_modifier:
        address += " " + street_number_modifier
    if street_direction:
        address += " " + street_direction
    if street_direction_suffix:
        address += " " + street_direction_suffix
    if street_name:
        address += " " + street_name
    if street_suffix:
        address += " " + street_suffix
    if street_suffix_modifier:
        address += " " + street_suffix_modifier
    if unit_number:
        address += " #" + unit_number

    return address

property_types = [
    ("SFR","Single Family Residence"),
    ("CND","Condominium"),
    ("TWN", "Townhouse"),
    ("LFT", "Loft"),
]

def clean_type_string(type):
    for item in property_types:
        if type in item[1]:
            return item[0]
    return "type"


cities = [
    ("RHE","Rolling Hills Estates"),
    ("RPV","Rancho Palos Verdes"),
    ("PVE", "Palos Verdes Estates"),
    ("TOR", "Torrance"),
    ("RB", "Redondo Beach"),
    ("HB", "Hermosa Beach"),
    ("MB", "Manhattan Beach"),
]

def clean_city_string(city):
    for item in cities:
        if city in item[1]:
            return item[0]
    return "city"


def correct_case(address):
    alphanumeric_only = [ a for a in address if a in string.ascii_letters ]
    uppercase_ratio = sum(x.isupper() for x in alphanumeric_only) / float(len(alphanumeric_only))
    if uppercase_ratio > 0.8:
        return address.title()
    else:
        return address


#################################################################
##################  Create Object Functions    ##################
#################################################################



def add_agent(mls_id, first_name, last_name, office_id, office_name,
              email, phone_number):
    """Create Agent object
    """
    mls_id = mls_id.upper()
    office_id = office_id.upper()
    first_name = correct_case(first_name)
    last_name = correct_case(last_name)
    office_name = correct_case(office_name)
    email = email.lower()

    try:
        agent = Agent.objects.get(mls_id=mls_id)
        if not agent.is_active:
            agent.first_name = first_name
            agent.last_name = last_name
            agent.office_id = office_id
            agent.office_name = office_name
            agent.email = email
            agent.phone_number = phone_number
            agent.save()   
        return agent
    except Agent.DoesNotExist:
        now = timezone.now()
        agent = Agent(email=email,
                      password="NoPWD",
                      mls_id=mls_id,
                      first_name=first_name,
                      last_name=last_name,
                      office_id=office_id,
                      office_name=office_name,
                      phone_number=phone_number,
                      is_active=False,
                      is_staff=False,
                      is_superuser=False,
                      last_login=now,
                      date_joined=now)
        agent.save()
        return agent
        
        
def add_property(mls_id, address, city, zip_code, latitude, longitude, beds, baths, price,
                  listing_agent, property_type, days_on_market, broker_open_id=1):
    """Create Property object
    """
    mls_id = mls_id.upper()
    address = correct_case(address)

    broker = BrokerOpen.objects.get(id=broker_open_id)
    try:
        property = Property.objects.get(mls_id=mls_id)
        property.address = address
        property.city = clean_city_string(city)
        property.zip_code = int_or_null(zip_code)
        property.latitude = decimal_or_null(latitude)
        property.longitude = decimal_or_null(longitude)
        property.beds = int_or_zero(beds)
        property.baths = int_or_zero(baths)
        property.price = int_or_zero(price)
        property.listing_agent = listing_agent
        property.property_type = clean_type_string(property_type)
        property.days_on_market = int_or_zero(days_on_market)
        property.broker_open = broker
        property.sort_order = BrokerOpen.get_next_sort_value(broker_open_id)
        property.save()
        return property
    except Property.DoesNotExist:
        property = Property.objects.create(mls_id=mls_id,
                                            start_time="11:00",
                                            end_time="13:00",
                                            address=address,
                                            city=clean_city_string(city),
                                            zip_code=int_or_null(zip_code),
                                            latitude=decimal_or_null(latitude),
                                            longitude=decimal_or_null(longitude),
                                            beds=int_or_zero(beds),
                                            baths=int_or_zero(baths),
                                            price=int_or_zero(price),
                                            listing_agent=listing_agent,
                                            property_type=clean_type_string(property_type),
                                            days_on_market=int_or_zero(days_on_market),
                                            broker_open=broker,
                                            sort_order=BrokerOpen.get_next_sort_value(broker_open_id)
                                            )
        property.save()
        return property                                                       
                                                
                                                                    
def import_row(row, broker_open_id):

    listing_agent = add_agent(row["Listing Agent Public ID"],
                               row["Listing Agent First Name"],
                               row["Listing Agent Last Name"],
                               row["Listing Office Code"],
                               row["Listing Office Name"],
                               row["Listing Agent Email Address"],
                               row["Listing Agent Cell Phone"],)
                               
    address = gen_address(row["Street Direction"], row["Street Direction Suffix"],
                            row["Street Name"], row["Street Number"], row["Street Number Modifier"],
                            row["Street Suffix"], row["Street Suffix Modifier"], row["Unit Number"])

    add_property(row["Multiple Listing Number"],
                    address, row["City"],
                    row["Zip Code"], row["Latitude"], row["Longitude"],
                    row["Bedrooms"], row["Baths Total"], row["Current Price"],
                    listing_agent, row["Type"], row["Days on Market"], broker_open_id)


def save_file(file):
    """
    Accepts a Python file-like object as arguement
    validates file ends in CSV, then saves it.
    returns import id on success, False on fail
    """

    #make sure file ends in .csv
    if '.csv' not in file.name[-4:].lower():
        print "File Not CSV"
        return False

    return True


def process_file(file, broker_open_id):
    """
    Import data from csv file
    returns dict { "success":BOOL, "details":STRING, "dump":STRING }
    """
    try:
        data = csv.DictReader(file)
        issublist(REQUIRED_FIELD, data.fieldnames) # gives error when called later on some files, see if error occurs.

    except Exception as e:
        return {'success':False, 'details':"An Error Occurred While Opening The File.", 'dump':str(e)}


    # make sure all the required fields are in the csvfile
    if not issublist(REQUIRED_FIELD, data.fieldnames):
        error_description = "Missing headers: %s" % ( ", ".join(items_missing(REQUIRED_FIELD, data.fieldnames)) )
        return {'success':False, 'details':"Missing Headers", 'dump':error_description}

    error=""
    dump=""
    error_rows = []

    for current_row, reader in enumerate(data):
        #print reader["Multiple Listing Number"]
        try:
            import_row(reader, broker_open_id)

        except Exception as e:
            error_rows.append(reader['Multiple Listing Number'])
            error_description = "Error Importing Row# %s (MLS# %s)\n\n" % (current_row, reader['Multiple Listing Number'] )
            error += error_description + "\n" + str(e)

    error_rows_string = "Error importing: %s" % ", ".join(error_rows)[:200] if len(error_rows) > 0 else "All rows imported successfully"
    return {'success':True, 'details': error_rows_string, 'dump':error}



