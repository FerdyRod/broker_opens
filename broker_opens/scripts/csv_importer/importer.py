import os
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../','..'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "broker.settings.base")

import file_handler

if __name__ == "__main__":
    try:
        a = sys.argv[1]
    except:
        print "Enter filename of import file as argument -- ex: python importer.py FILE.CSV"
        sys.exit(0)

    print "Starting file processor"
    process_result = file_handler.process_file(open(a, "r"))
    if process_result['success']:
        print "Import Completed Successfully"
        print process_result['details']
        print process_result['dump']
