from .base import *

#DEBUG
DEBUG = True
TEMPLATE_DEBUG = DEBUG

#ALLOWED_HOSTS
ALLOWED_HOSTS = ['127.0.0.1',]

#INSTALLED_APP plus django-debug-toolbar
INSTALLED_APPS += (
    'debug_toolbar',
)

#MIDDLEWARE_CLASSES puls debug-toolbar middleware
MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

#EMAIL CONFIGURATION
EMAIL_HOST = 'mail.c3csolutions.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'noreply@c3csolutions.com'
EMAIL_HOST_PASSWORD = '72w^uL4n'
DEFAULT_FROM_EMAIL = 'noreply@c3csolutions.com'

#Internal IP for debug-toolbar
INTERNAL_IPS = ('127.0.0.1',)
