from django.conf.urls import patterns, include, url
from django.contrib.auth.views import login, logout
from django.conf import settings
from django.contrib import admin
from tastypie.api import Api

from broker_opens import views
from django.views.generic.base import TemplateView
from broker_opens.views import RegistrationView, ActivationView
from broker_opens.api import PropertyResource, AgentResource


v1_api = Api(api_name='v1')
v1_api.register(AgentResource())
v1_api.register(PropertyResource())

admin.autodiscover()

urlpatterns = patterns('',

    url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.STATIC_ROOT}),
    url(r'^$', 'broker_opens.views.home', name='home'),
    url(r'^(?P<month>\d{2})/(?P<day>\d{2})/(?P<year>\d{2})/(?P<location>.+)/$',
        'broker_opens.views.property_list', name='property_list'),
    url(r'^new/$', 'broker_opens.views.new_broken_open', name='new_broken_open'),
    url(r'^broker_open/(.+)$', 'broker_opens.views.edit_broker_open', name='edit_broker_open'),
    url(r'^delete/(.+)$', 'broker_opens.views.delete_broker_open', name='delete_broker_open'),
    url(r'^my_listings/$', 'broker_opens.views.users_listings', name='users_listings'),
    url(r'^create_listing/$', 'broker_opens.views.create_users_listing', name='create_listings'),
    url(r'^create_listing/(.+)$', 'broker_opens.views.moderator_create_listing', name='moderator_create_listing'),
    url(r'^upload_csv/(.+)$', 'broker_opens.views.upload_csv', name='upload_csv'),
    url(r'^my_info/$', 'broker_opens.views.edit_users_profile', name='edit_profile'),
    url(r'^edit_agent_info/(.+)$', 'broker_opens.views.moderator_edit_users_profile', name='moderator_edit_profile'),
    url(r'^create_agent/$', 'broker_opens.views.moderator_create_user', name='moderator_create_user'),
    url(r'^agent/edit_listing/(.+)$', 'broker_opens.views.edit_listing', name='edit_listing'),
    url(r'^moderator/agents/$', 'broker_opens.views.list_agents', name='agent_list'),
    url(r'^password_reset/$', 'django.contrib.auth.views.password_reset',
        {'template_name': 'password_reset/password_reset_form.html',
         'email_template_name':'password_reset/password_reset_email.html'}, name='password_reset'),
    url(r'^password/reset_done/$', 'django.contrib.auth.views.password_reset_done',
        {'template_name': 'password_reset/password_reset_done.html'}, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm',
        {'template_name': 'password_reset/password_reset_confirm.html', 'post_reset_redirect' : '/reset/done/'},
        name='password_reset_confirm'),
    url(r'^reset/done/$', 'django.contrib.auth.views.password_reset_complete',
        {'template_name': 'password_reset/password_reset_complete.html'}),
    url(r'^contact/$', 'broker_opens.views.contact', name='contact'),
    url(r'^thanks/$', 'broker_opens.views.thanks', name='thanks'),
    url(r'^login/$', 'broker_opens.views.log_in', name='login'),
    url(r'^logout/$', logout, {'next_page':'login'}, name='logout'),
    url(r'^account_verification/$', views.verification, name='verifying_account'),
    url(r'^accounts/activate/(?P<activation_key>\w+)/$', ActivationView.as_view(), name='registration_activate'),
    url(r'^accounts/register/$', RegistrationView.as_view(), name='registration_register'),
    url(r'^accounts/activate/complete/$', TemplateView.as_view(template_name='registration/activate.html'), name='registration_activation_complete'),
    url(r'^accounts/register/complete/$', TemplateView.as_view(template_name='registration/registration_complete.html'), name='registration_complete'),
    url(r'^admin/', include(admin.site.urls)),

    ##url(r'^api/', include(v1_api.urls)),
)
